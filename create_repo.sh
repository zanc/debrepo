#!/bin/bash

debs_paths=(
	"$(cd ../cd-firmware/pool && pwd)"
	#"/mnt/debian/var/cache/apt/archives"
	"/var/cache/apt/archives"
)
suite=stretch

if ! which reprepro >/dev/null 2>&1; then
	echo "$0: reprepro: command not found" >&2
	exit 1
fi

found=true
for i in "${debs_paths[@]}"; do
	if [ -z "$(find "$i" -type f -name '*.deb')" ]; then
		found=false
		echo "$0: $i: doesn't contain any .deb file" >&2
	fi
done
if ! $found; then
	exit 1
fi

rm -rf db dists pool
for i in "${debs_paths[@]}"; do
	find "$i" -type f -name '*.deb' -exec reprepro includedeb $suite {} + 2>/dev/null
done
